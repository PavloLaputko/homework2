import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Test {
    //Data for login
    String baseURL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    String login = "webinar.test@gmail.com";
    String password = "Xcg7299bnSmMuRLp9ITw";

    //Webdriver initialization
    WebDriver driver = new ChromeDriver();


    void loginAsAdmin(){
        driver.get(baseURL);
        //Base URL page elements
        WebElement emailField = driver.findElement(By.id("email"));
        WebElement passwordField = driver.findElement(By.id("passwd"));
        WebElement vhodButton = driver.findElement(By.name("submitLogin"));
        //Login
        emailField.sendKeys(login);
        passwordField.sendKeys(password);
        vhodButton.click();
    }
    /*-------------------------------------------------------------------------------
       LOGIN TO THE ADMIN PANEL
    ---------------------------------------------------------------------------------*/
        void scriptA() {
        //1.Main page opening
        driver.get(baseURL);
        //2. Log in
        loginAsAdmin();
        // Wait for page load
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Find admin page  elements
        WebElement userPic = driver.findElement(By.className("imgm"));
        WebElement logout = driver.findElement(By.id("header_logout"));
        //3.User pic clicking and log out from the page
        userPic.click();
        logout.click();
    }
    /*-------------------------------------------------------------------------------
       ADMIN PANEL ITEMS CHECKING
    ---------------------------------------------------------------------------------*/
    void scriptB(){
        //1.Log in
        driver.get(baseURL);
        loginAsAdmin();
        // Wait for page full download
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        //Menu initialization
        WebElement menu = driver.findElement(By.className("menu"));
        List<String> menuUrls = new ArrayList<String>();
        List<WebElement> links = menu.findElements(By.cssSelector("li.maintab > a"));
        //Filling up menuUrls with menu links
        for (WebElement e:links ) {
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            menuUrls.add(e.getAttribute("href"));
        }
        //2. Clicking on menu items
        for (String s: menuUrls) {
            driver.get(s);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            //2a. Menu items showing in console
            String title = driver.getTitle();
            System.out.println(title);
            //2b. Page refreshing and titles assert
            driver.navigate().refresh();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            assert title.equals(driver.getTitle());

        }
        driver.quit();
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.scriptA();
        test.scriptB();
    }
}
